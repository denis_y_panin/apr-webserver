#ifndef STATIC_GENERATOR_H
#define STATIC_GENERATOR_H

#include "IResponseGenerator.h"
#include <memory>
#include <unordered_map>
#include <boost/filesystem.hpp>

class StaticGenerator :
    public IResponseGenerator
{
public:
    StaticGenerator(void);
    virtual ~StaticGenerator(void);
    virtual void process_request(const std::map<std::string, std::string> &request, std::vector<char> &output, const std::string &root_path = "/");
    virtual const char* get_name() const;
    virtual const char* get_version() const;
    void generate_error_response(std::vector<char> &output, const std::string &error_code);

private:
    std::string get_mime_type(const boost::filesystem::path &path) const;
    void add_common_headers(std::string &output, const std::string &filepath = "");
    void generate_get_response(std::vector<char> &output, const std::string &filepath);

    static const std::unordered_map<std::string, std::string> mime_types;
    static std::unordered_map<std::string, std::string> fill_types();
};


#endif STATIC_GENERATOR_H