#include "stdafx.h"
#include "StaticGenerator.h"
#include <fstream>
#include <locale>
#include <string>
#include <sstream>
#include <regex>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string/find.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lexical_cast.hpp>


const std::unordered_map<std::string, std::string> StaticGenerator::mime_types = StaticGenerator::fill_types();

const char* StaticGenerator::get_name() const
{
    return "Static";
}

const char* StaticGenerator::get_version() const
{
    return "1.0";
}

void StaticGenerator::process_request(const std::map<std::string, std::string> &request, std::vector<char> &output, const std::string &root_path)
{
    if(request.at("Method") == "GET")
        generate_get_response(output, root_path);
    else
        generate_error_response(output, "405 Method Not Allowed");
}

void StaticGenerator::add_common_headers(std::string &output, const std::string &filepath)
{
    std::ostringstream date_time;
    
    date_time.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%a, %d %b %Y %H:%M:%S GMT")));
    date_time << boost::posix_time::second_clock::universal_time();

    output.append("\r\nDate: " + date_time.str());
    output.append("\r\nServer: MFS");
    output.append("\r\nContent-Type: " + (filepath == "" ? "text/html" : get_mime_type(boost::filesystem::path(filepath))));
}

void StaticGenerator::generate_get_response(std::vector<char> &output, const std::string &filepath)
{
    std::string answer_headers = "";
    
    boost::filesystem::path path(filepath);
    std::ifstream fin(filepath, std::ios::in|std::ios::binary);
    std::ostringstream date_time;
    
    date_time.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%a, %d %b %Y %H:%M:%S GMT")));
    if(boost::filesystem::exists(path))
        if(fin.is_open())
        {
            answer_headers.append("HTTP/1.1 200 OK");

            add_common_headers(answer_headers, filepath);
            date_time << boost::posix_time::from_time_t(boost::filesystem::last_write_time(path));

            answer_headers.append("\r\nLast-Modified: " + date_time.str());
            answer_headers.append("\r\nContent-Length: " + boost::lexical_cast<std::string>(boost::filesystem::file_size(path)));
            answer_headers.append("\r\n\r\n");

            output.insert(output.begin(), answer_headers.begin(), answer_headers.end());
            std::string contents((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
            output.insert(output.end(),contents.begin(), contents.end());
        }
        else
            generate_error_response(output, "424 Method Failure");
    else
        generate_error_response(output, "404 Not Found");

    fin.close();
}

void StaticGenerator::generate_error_response(std::vector<char> &output, const std::string &error_code)
{
    std::string answer = "";
    std::string body = "<!DOCTYPE html><html><body><h1>"+ error_code + "</h1></body></html>";

    answer.append("HTTP/1.1 "+ error_code);
    add_common_headers(answer);
    answer.append("\r\nContent-Length: " + boost::lexical_cast<std::string>(body.size()));
    answer.append("\r\n\r\n");
    answer.append(body);

    output.insert(output.end(), answer.begin(), answer.end());
}

std::string StaticGenerator::get_mime_type(const boost::filesystem::path  &path) const
{
    std::string filetype (path.filename().string());
    std::size_t pos (filetype.rfind(".") + 1);

    if (pos == std::string::npos)
        return "";
  
    filetype = filetype.substr(pos);
    boost::algorithm::to_lower(filetype);

    return mime_types.at(filetype);
}

std::unordered_map<std::string, std::string> StaticGenerator::fill_types()
{
    std::unordered_map<std::string, std::string> tmp_map;
    
    tmp_map["ini"] = "text/plain";
    tmp_map["txt"] = "text/plain";
    tmp_map["conf"] = "text/plain";
    tmp_map["js"] = "application/javascript";
    tmp_map["json"] = "application/json";
    tmp_map["css"] = "text/css";
    tmp_map["html"] = "text/html";
    tmp_map["htm"] = "text/html";
    tmp_map["xml"] = "text/xml";
    tmp_map["csv"] = "text/csv";
    tmp_map["rtf"] = "text/rtf";
    tmp_map["jpeg"] = "image/jpeg";
    tmp_map["jpg"] = "image/jpeg";
    tmp_map["bmp"] = "image/x-ms-bmp";
    tmp_map["gif"] = "image/gif";
    tmp_map["png"] = "image/png";
    tmp_map["tiff"] = "image/tiff";
    tmp_map["ogg"] = "audio/ogg";
    tmp_map["mp3"] = "audio/mpeg";
    tmp_map["avi"] = "video/x-msvideo";
    tmp_map["pdf"] = "application/pdf";
    tmp_map["doc"] = "application/msword";
    tmp_map["swf"] = "application/x-shockwave-flash";
    tmp_map["xls"] = "application/vnd.ms-excel";
    tmp_map["zip"] = "application/zip";
    tmp_map["tar"] = "application/x-tar";
    tmp_map["pl"] = "application/x-perl";
    tmp_map["zip"] = "application/zip";
    tmp_map["py"] = "application/x-python";
    
    tmp_map["exe"] = " ";
    tmp_map["chm"] = " ";
    tmp_map["obj"] = " ";
    tmp_map["pyd"] = " ";
    tmp_map["asm"] = " ";
    tmp_map["dll"] = " ";
    tmp_map["lib"] = " ";
    tmp_map["dep"] = " ";
    tmp_map["sqm"] = " ";
    tmp_map["suo"] = " ";
    tmp_map["sys"] = " ";
    tmp_map["pdb"] = " ";
    tmp_map["idb"] = " ";
    tmp_map["idb"] = " ";
    tmp_map["sbr"] = " ";
    
    return tmp_map;
}

StaticGenerator::StaticGenerator(void)
{
    
}


StaticGenerator::~StaticGenerator(void)
{
}
