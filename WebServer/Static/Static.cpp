// Static.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "StaticGenerator.h"


extern "C"
{
    // Plugin factory function
    __declspec(dllexport) IResponseGenerator* Create_Plugin ()
    {
        return new StaticGenerator ();
    }
}