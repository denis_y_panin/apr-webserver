#include "Server.h"

#include "WinSockWrapper.h"
#include "RequestParser.h"
#include "Config.h"
#include "Connection.h"
#include "Settings.h"
#include "Logger.h"
#include "Utils.h"
#include "PluginsRepository.h"
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <boost/scoped_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <tbb/task.h>
#include <tbb/task_group.h>
#include <tbb/task_scheduler_init.h>
#include "ListenTask.h"
#include "ListenRoot.h"

Server::Server()
{

}

int Server::run()
{

    WSADATA wsaData;
    int init_result;
    Logger::log_manual("Server start routine initiated", 0, notification, ServerModule);

    std::auto_ptr<Settings> settings(Settings::CreateSettings(CFG_CONFIGFILE_PATH));

    PluginsRepository::load();
    Logger::log_manual("Plugins were loaded successfully", 0, notification, ServerModule);

	init_result = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (init_result != 0) 
    {
        int wsaerror = WSAGetLastError();
        throw(ServerException("Server failed to start: WSAStartup failed with error", wsaerror, critical, ServerModule));
    }
    Logger::log_manual("WSA initiated", ServerModule);
    _continuation_flag = true;
    
    tbb::task_scheduler_init init(settings->tbb_concurrency);
    root = new(tbb::task::allocate_root()) ListenRoot(_continuation_flag);
    tbb::task::spawn_root_and_wait(*root);
	return 0;
}

int Server::stop()
{
    Logger::log_manual("Server stop routine initiated", 0, notification, ServerModule);
    _continuation_flag = false;
    root->wait_for_all();
    root->group()->cancel_group_execution();
    Logger::log_manual("All opened tasks closed", ServerModule);
    WSACleanup();
    PluginsRepository::clean_up();
    Logger::log_manual("Plugins unloaded", ServerModule);
    Logger::log_manual("Served stopped successfully", 0, notification, ServerModule);
    Logger::shutdown();
	return 0;
}

int Server:: restart()
{
    stop();
    run();
    return 0;
}

Server::~Server()
{
    if (_continuation_flag)
        stop();
}

