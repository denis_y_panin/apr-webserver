#ifndef CONFIG_H
#define CONFIG_H

static const char* CFG_CONFIGFILE_PATH = "config.cfg";
static const size_t CFG_BUFLEN = 1024;
static const int CFG_ALLOWED_INCOMPLETE_REQUESTS_COUNT = 3; 

static const int CFG_TBB_CONCURRENCY = 20;
static const char* CFG_PATH_TO_PLUGINS = "plugins";
static const int CFG_HTTP_PORT = 4080;
static const int CFG_HTTPS_PORT = 4443;
static const char* CFG_VERSION = "1.1";
static const char* CFG_REQUEST = "GET";
static const char* CFG_PATH_TO_SERVER_SERTIFICATE = "certs/mars-server.crt";
static const char* CFG_PATH_TO_SERVER_PRIVATE_KEY = "certs/mars-server.key";
static const char* CFG_PATH_TO_CA_CERTIFICATE = "certs/ca.crt";
static const char* CFG_PATH_TO_DHPARAMS_FILE = "certs/dh1024.pem";
static const char* CFG_PATH_TO_LOG_DIR = "Logs";
static const char* CFG_ERROR_LOG_NAME_TEMPLATE = "Important";
static const int CFG_ERROR_LOG_ROTATE_ON_BYTES = 1048576;
static const char* CFG_WARNING_LOG_NAME_TEMPLATE = "";
static const int CFG_WARNING_LOG_ROTATE_ON_BYTES = 0;
static const char* CFG_WORKING_LOG_NAME_TEMPLATE = "Working";
static const int CFG_WORKING_LOG_ROTATE_ON_BYTES = 1048576;
static const char* CFG_PATH_TO_ROUTES = "C:/repository";
static const char* CFG_URL1 = "((http://)?(www.)?/testsite.com)(.*)";
static const char* CFG_SOURCE1 = "Static";
static const char* CFG_NAME1 = "/testsite";
static const char* CFG_URL2 = "/";
static const char* CFG_SOURCE2 = "CGI";
static const char* CFG_NAME2 = "hello.php";
static const int CFG_MAX_REQUEST_SIZE = 10485760;
static const char* CFG_UNRECOGNIZED_PATTERN = "Unrecognized";
static const char* CFG_HEADERS[16] ={"Host",
					                "Connection",
					                "Accept",
					                "User-Agent",
					                "Accept-Encoding",
					                "Accept-Language",
					                "Cookie",
					                "Accept-Charset",
					                "Content-Encoding",
					                "Content-Language",
					                "Content-Length",
					                "Content-Location",
					                "Content-MD5",
					                "Content-Range",
					                "Content-Type",
                                    "Cache-Control"};
#endif

