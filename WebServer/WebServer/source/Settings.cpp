#include "Settings.h"
#include "Config.h"

Settings::Settings(const std::string & path)
{
    #ifndef CONFIG_SHORT
        config.readFile(path.c_str());
        init_settings();
    #else
        init_settings_shortcut();
    #endif
}

void Settings::init_settings_shortcut()
{
    tbb_concurrency = CFG_TBB_CONCURRENCY;
    path_to_plugins = CFG_PATH_TO_PLUGINS;
    http_ports.push_back(CFG_HTTP_PORT);
    https_ports.push_back(CFG_HTTPS_PORT);
    std::string str = CFG_VERSION;

    str.erase(std::remove(str.begin(), str.end(), '.'), str.end());
    if(str.length() == 2)
	    std::copy(str.begin(), str.end(), http_version);

    allowed_requests.insert(CFG_REQUEST);
    path_to_server_certificate = CFG_PATH_TO_SERVER_SERTIFICATE;
    path_to_server_private_key = CFG_PATH_TO_SERVER_PRIVATE_KEY;
    path_to_ca_certificate = CFG_PATH_TO_CA_CERTIFICATE;
    path_to_dhparams_file = CFG_PATH_TO_DHPARAMS_FILE;
    path_to_log_dir = CFG_PATH_TO_LOG_DIR;
    error_log_name_template = CFG_ERROR_LOG_NAME_TEMPLATE;
    error_log_rotate_on_bytes = CFG_ERROR_LOG_ROTATE_ON_BYTES;
    warning_log_name_template = CFG_WARNING_LOG_NAME_TEMPLATE;
    warning_log_rotate_on_bytes = CFG_WARNING_LOG_ROTATE_ON_BYTES;
    working_log_name_template = CFG_WORKING_LOG_NAME_TEMPLATE;
    working_log_rotate_on_bytes = CFG_WORKING_LOG_ROTATE_ON_BYTES;
    path_to_routes = CFG_PATH_TO_ROUTES;
    std::string url;
	std::vector<std::string> value(2);
    url = CFG_URL1;
    value[0] = CFG_SOURCE1;
    value[1] = CFG_NAME1;
	routes[url] = value;

    url = CFG_URL2;
    value[0] = CFG_SOURCE2;
    value[1] = CFG_NAME2;
	routes[url] = value;

    max_request_size = CFG_MAX_REQUEST_SIZE;
    unrecognized_pattern = CFG_UNRECOGNIZED_PATTERN;
    for (int i=0;i<16;++i)
    {
        headers.push_back(CFG_HEADERS[i]);
    }
}

void Settings::init_settings()
{
    config.lookupValue("server.server_settings.tbb_concurrency", tbb_concurrency);                                                              // general server settings
    config.lookupValue("server.server_settings.path_to_plugins", path_to_plugins);

    const libconfig::Setting &http_ports_settings = config.lookup("server.server_settings.http_ports");
    for(int i = 0; i < http_ports_settings.getLength(); ++i)
            http_ports.push_back(http_ports_settings[i]);

    const libconfig::Setting &https_ports_settings = config.lookup("server.server_settings.https_ports");
    for(int i = 0; i < https_ports_settings.getLength(); ++i)
            https_ports.push_back(https_ports_settings[i]);

    std::string str = "";                                                                                                                                                               // http settings

    config.lookupValue("server.http_settings.version", str);
    str.erase(std::remove(str.begin(), str.end(), '.'), str.end());
    if(str.length() == 2)
            std::copy(str.begin(), str.end(), http_version);

    const libconfig::Setting &requests_settings = config.lookup("server.http_settings.supported_requests");
    for(int i = 0; i < requests_settings.getLength(); ++i)
            allowed_requests.insert(allowed_requests.end(),requests_settings[i]);

    config.lookupValue("server.ssl_settings.path_to_server_certificate", path_to_server_certificate);   // ssl settings
    config.lookupValue("server.ssl_settings.path_to_server_private_key", path_to_server_private_key);
    config.lookupValue("server.ssl_settings.path_to_ca_certificate", path_to_ca_certificate);
    config.lookupValue("server.ssl_settings.path_to_dhparams_file", path_to_dhparams_file);

    config.lookupValue("server.log_settings.path_to_log_dir", path_to_log_dir);                                                 // logging settings
    config.lookupValue("server.log_settings.error_log_name_template", error_log_name_template);
    config.lookupValue("server.log_settings.error_log_rotate_on_bytes", error_log_rotate_on_bytes);
    config.lookupValue("server.log_settings.warning_log_name_template", warning_log_name_template);
    config.lookupValue("server.log_settings.warning_log_rotate_on_bytes", warning_log_rotate_on_bytes);
    config.lookupValue("server.log_settings.working_log_name_template", working_log_name_template);
    config.lookupValue("server.log_settings.working_log_rotate_on_bytes", working_log_rotate_on_bytes);

    config.lookupValue("server.routing_table.path_to_routes", path_to_routes);                                                  // routing table
    libconfig::Setting &routes_settings = config.lookup("server.routing_table.routes");
    for(int i = 0; i < routes_settings.getLength(); ++i)
    {
            std::string url;
            std::vector<std::string> value(2);
            routes_settings[i].lookupValue("url", url);
            routes_settings[i].lookupValue("source", value[0]);
            routes_settings[i].lookupValue("name", value[1]);
            routes[url] = value;
    }
    config.lookupValue("server.parser.max_size", max_request_size);     
    config.lookupValue("server.parser.unrecognized_pattern", unrecognized_pattern);
    libconfig::Setting &supported_headers = config.lookup("server.parser.headers");
    for(int i = 0; i < supported_headers.getLength(); ++i)
    {
        headers.push_back(supported_headers[i]);
    }
}

Settings::~Settings(void)
{
}
