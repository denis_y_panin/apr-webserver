#ifndef _REQUEST_
#define _REQUEST_

#include <string>  
#include <set>
#include <map>  
#include <vector>  


class RequestParser
{
public:
    RequestParser(void);
    ~RequestParser(void);

    static void parse(const std::vector<char>&, std::map<std::string,std::string> & output);
 
    static void output(const std::map<std::string,std::string> &input);
private:
    static bool _add(const std::string &input,
                   std::vector<std::string>& headers,  
                   std::map<std::string,std::string>& output, 
                   const std::string &unrec_pattern,
                   int & iter);

    static void _unify(std::string &input); //some rfc magic

    static void _split_by_lines(const std::string &input, std::vector<std::string> &out_lines);
    static void _split_first_line(const std::string &input,
                                       std::string &out_method, 
                                       std::string &out_URI, 
                                       std::string &out_version);
};
#endif //!_REQUEST_
