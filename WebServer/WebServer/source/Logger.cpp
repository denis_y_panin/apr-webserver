#include "Logger.h"

#include <ostream>
#include <sstream>
#include <boost/make_shared.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/core.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/value_ref.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/sources/basic_logger.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/attributes/scoped_attribute.hpp>
#include <boost/log/trivial.hpp>
#include <memory>
#include "Config.h"
#include "Settings.h"
namespace logging = boost::log;
namespace src = boost::log::sources;
namespace expr = boost::log::expressions;
namespace sinks = boost::log::sinks;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

BOOST_LOG_ATTRIBUTE_KEYWORD(line_id, "LineID", unsigned int)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", severity_level)
BOOST_LOG_ATTRIBUTE_KEYWORD(tag_attr, "Tag", std::string)

typedef boost::log::sinks::asynchronous_sink< boost::log::sinks::text_file_backend > file_sink;

std::ostream& operator<< (std::ostream& strm, severity_level level)
{
    static const char* strings[] =
    {
        "normal",
        "notification",
        "warning",
        "error",
        "critical"
    };

    if (static_cast< std::size_t >(level) < sizeof(strings) / sizeof(*strings))
        strm << strings[level];
    else
        strm << static_cast< int >(level);

    return strm;
}
std::ostream& operator<< (std::ostream& strm, server_module module)
{
    static const char* strings[] =
    {
        "WinSockWrapper",
        "SSLWrapper",
        "MessageParsing",
        "MessageProcessing",
        "ServerModule",
        "ConfigLoading",
        "Plugins",
        "General"
    };

    if (static_cast< std::size_t >(module) < sizeof(strings) / sizeof(*strings))
        strm << strings[module];
    else
        strm << static_cast< int >(module);

    return strm;
}

Logger::Logger(void)
{
}


Logger::~Logger(void)
{

}
void Logger::log_exception(const ServerException &exception) 
{
    log_manual(exception.what(),exception.get_error(),exception.get_severity(),exception.get_module());
}

void Logger::log_exception(const libconfig::ConfigException &exception) 
{
    log_manual(exception.what(), 0, critical, ConfigLoading);
}
//-----------
void Logger::log_manual(
                std::string message, 
                int error, 
                severity_level severity,
                server_module module)
{
    src::severity_logger< severity_level > slg;
    if (error)
    {
        BOOST_LOG_SEV(slg, severity) << "["<< module <<"] " << message << " [error: "<< error <<"]";
    }else
    {
        BOOST_LOG_SEV(slg, severity) << "["<< module <<"] " << message;
    }
}
//-----------
void Logger::log_manual_tagged(
                std::string message, 
                int error, 
                severity_level severity,
                server_module module,
                std::string tag) 
{
    src::severity_logger< severity_level > slg;
    BOOST_LOG_SCOPED_THREAD_TAG("Tag", tag);
    if (error)
    {
        BOOST_LOG_SEV(slg, severity) << "["<< module <<"] " << message << " [error: "<< error <<"]";
    }else
    {
        BOOST_LOG_SEV(slg, severity) << "["<< module <<"] " << message;
    }
}
//-----------
void Logger::log_manual(std::string message) //error 0, normal, general
{
    log_manual(message, 0, normal, General);
}
//-----------
void Logger::log_manual(std::string message, server_module module) // //error 0, normal
{
    log_manual(message, 0, normal, module);
}
//-----------
void Logger::log_manual_tagged(std::string message, std::string tag) //error 0, normal, general
{
    log_manual_tagged(message, 0, normal, General, tag);
}
//-----------
void Logger::log_manual_tagged(std::string message, server_module module, std::string tag) // //error 0, normal
{
    log_manual_tagged(message, 0, normal, module, tag);
}
//-----------
int Logger::init() 
{
    std::auto_ptr<Settings> myset(Settings::CreateSettings(CFG_CONFIGFILE_PATH));
    std::string path_to_log_dir =  myset->path_to_log_dir;
    std::string error_log_name_template = myset->error_log_name_template;
    unsigned int error_log_rotate_on_bytes = myset->error_log_rotate_on_bytes;
    std::string working_log_name_template = myset->working_log_name_template;
    unsigned int working_log_rotate_on_bytes = myset->working_log_rotate_on_bytes;
    
    logging::formatter fmt = expr::stream
        << std::setw(6) << std::setfill('0') << line_id << std::setfill(' ')
        << "[" << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f") << "]" 
        << ": <" << severity << "> "
        << expr::if_(expr::has_attr(tag_attr))
            [
                expr::stream << "%->" << tag_attr << "<-% "
            ]
        << expr::smessage;
    
    boost::shared_ptr< file_sink > file_sink = _make_sink(working_log_name_template, path_to_log_dir+"/WorkingLogs", working_log_rotate_on_bytes, fmt);
    
    logging::core::get()->add_sink(file_sink);
    
    file_sink = _make_sink(error_log_name_template, path_to_log_dir+"/ImportantLogs",error_log_rotate_on_bytes, fmt);
     
    file_sink->set_filter(severity >= warning || (expr::has_attr(tag_attr)));
       
    logging::core::get()->add_sink(file_sink);
    
    logging::add_common_attributes();
    
    return 0;
}

boost::shared_ptr< boost::log::sinks::asynchronous_sink< boost::log::sinks::text_file_backend > > Logger::_make_sink(
                                                                                                                       std::string name_template,
                                                                                                                       std::string path_to_log_dir,
                                                                                                                       unsigned int rotate_on_bytes,
                                                                                                                       const boost::log::formatter &fmt)
{
    boost::shared_ptr< file_sink > file_sink(new file_sink(
        keywords::file_name = name_template+"_%2N.log",      
        keywords::open_mode = (std::ios::out | std::ios::app), 
        keywords::rotation_size = rotate_on_bytes           
    ));
    
    file_sink->locked_backend()->set_file_collector(sinks::file::make_collector(
        keywords::target = path_to_log_dir,                          
        keywords::max_size = 10*1024*1024,              
        keywords::min_free_space = 200 * 1024 * 1024        
    ));
    file_sink->locked_backend()->scan_for_files();
    file_sink->set_formatter(fmt);
    file_sink->locked_backend()->auto_flush(true);
    return file_sink;
}

void Logger::shutdown()
{
    boost::shared_ptr< logging::core > core = logging::core::get();
    core->flush();
    core->remove_all_sinks();
}