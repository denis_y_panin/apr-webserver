#ifndef LISTEN_ROOT
#define LISTEN_ROOT

#include <tbb/task.h>
#include <vector>
#include <tbb/atomic.h>

class ListenRoot : public tbb::task
{
public:
    ListenRoot(tbb::atomic<bool>& continuation_flag);
    tbb::task* execute();
private:
    std::vector<unsigned int> _http_ports;
    std::vector<unsigned int> _https_ports;
    tbb::atomic<bool>& _continuation_flag;
};

#endif //!LISTEN_ROOT