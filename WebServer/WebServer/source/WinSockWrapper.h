#ifndef WIN_SOCK_WRAPPER
#define WIN_SOCK_WRAPPER

#ifndef I_WIN_SOCK_WRAPPER
#include "iwinsockwrapper.h"
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN  
#endif 

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vector>  

class WinSockWrapper :
    public IWinSockWrapper
{
public:
    WinSockWrapper(SOCKET ClientSocket, unsigned BufferLength);
    ~WinSockWrapper(void);

private:

    SOCKET _ClientSocket;
    unsigned _BufferLength;

    int _load(SOCKET ClientSocket, unsigned BufferLength);
    int _send_message(const std::vector<char>& message);
    int _recieve_message(std::vector<char>& message);
    int _close();
};
#endif //!WIN_SOCK_WRAPPER
