#include "WinSockWrapper.h"
#include "ServerException.h"
#include "Logger.h"

WinSockWrapper::WinSockWrapper(SOCKET ClientSocket, unsigned BufferLength) 
{
    _load(ClientSocket, BufferLength);
}


WinSockWrapper::~WinSockWrapper(void)
{
    close();
}

int WinSockWrapper::_load(SOCKET ClientSocket, unsigned BufferLength)
{
    if (ClientSocket == INVALID_SOCKET)
       throw ServerException("Load socket failed", 0, warning, WSWrapper);
   
    _ClientSocket = ClientSocket;
    _BufferLength = BufferLength;
    return 0;
}
int WinSockWrapper::_send_message(const std::vector<char>& message)
{
    int SendResult = send( _ClientSocket, message.data(), message.size(), 0 ); 
    if (SendResult == SOCKET_ERROR)
    {
        close();
        int l = WSAGetLastError();
        throw ServerException("Send message failed, l, warning, WSWrapper)");
    }
    return SendResult;
}
int WinSockWrapper::_recieve_message(std::vector<char>& message)
{
    Logger::log_manual("Winsock connection opened successfully", 0, normal, WSWrapper);

    if (message.size()<_BufferLength) //adjusting size of the buffer to buffer length
    {
        message.resize(_BufferLength);
    }
    int ReceiveResult = recv(_ClientSocket, message.data(), _BufferLength, 0); //returns size of the received message
    if (ReceiveResult == SOCKET_ERROR)
    {
        close();
        int l = WSAGetLastError();
        throw ServerException("Receive message failed", l, warning, WSWrapper);
    }
    message.resize(ReceiveResult); //adjusting size of the buffer to actual size of the message
    return ReceiveResult;
}
int WinSockWrapper::_close() 
{
    shutdown(_ClientSocket, SD_BOTH);
    closesocket(_ClientSocket);
    Logger::log_manual("Winsock connection closed successfully", 0, normal, WSWrapper);
    return 0;
}