#include "ListenRoot.h"
#include "Settings.h"
#include "Config.h"
#include "ListenTask.h"

ListenRoot::ListenRoot(tbb::atomic<bool>& continuation_flag) 
    : _continuation_flag(continuation_flag)
{
    _http_ports = Settings::CreateSettings(CFG_CONFIGFILE_PATH)->http_ports;
    _https_ports = Settings::CreateSettings(CFG_CONFIGFILE_PATH)->https_ports;
}

tbb::task* ListenRoot::execute()
{      
    this->set_ref_count(_http_ports.size() + _https_ports.size() + 1);

    tbb::task_list listen_tasks;

    for (auto it = _http_ports.begin(); it != _http_ports.end(); ++it)
    {
        listen_tasks.push_back( *new(tbb::task::allocate_child()) ListenTask(*it, false, _continuation_flag) );
    }

    for (auto it = _https_ports.begin(); it != _https_ports.end(); ++it)
    {
        listen_tasks.push_back( *new(tbb::task::allocate_child()) ListenTask(*it, true, _continuation_flag) );
    }

    this->spawn_and_wait_for_all(listen_tasks);

    return NULL;
}

