
#pragma once
#pragma comment (lib, "libconfig++.lib")

#ifndef SETTINGS_H
#define SETTINGS_H

#define CONFIG_SHORT 1

#pragma warning( disable: 4290 )
#include <libconfig\libconfig.h++>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>


struct Settings
{
    // general settings
    unsigned int tbb_concurrency; 
    std::string path_to_plugins; 
    std::vector<unsigned int> http_ports; 
    std::vector<unsigned int> https_ports; 
    
    //http settings
    unsigned char http_version[2];		 
    std::set<std::string> allowed_requests; 
    
    //ssl settings
    std::string path_to_server_certificate;
    std::string path_to_server_private_key;
    std::string path_to_ca_certificate;
    std::string path_to_dhparams_file;
    
    //routing table 
    std::string path_to_routes; 
    std::map<std::string,std::vector<std::string>> routes; 
    
    //logging settings
    std::string path_to_log_dir; 
    std::string error_log_name_template; 
    unsigned int error_log_rotate_on_bytes;
    std::string warning_log_name_template; 
    unsigned int warning_log_rotate_on_bytes; 
    std::string working_log_name_template; 
    unsigned int working_log_rotate_on_bytes;
    
    //parser settings
    unsigned int max_request_size;
    std::string unrecognized_pattern;
    std::vector<std::string> headers;
    
    ~Settings(void);
    static std::auto_ptr<Settings> CreateSettings(const std::string &path)
    {
        return std::auto_ptr<Settings>(new Settings(path));
    }
    
private: 
    explicit Settings(const std::string &path);
    libconfig::Config config;
    void init_settings();
    void init_settings_shortcut();
    
};

#endif