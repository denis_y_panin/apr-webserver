
#include "RequestParser.h"

#include <algorithm>  
#include <sstream> 
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/utility.hpp>
#include <boost/assign/list_of.hpp> 
#include <boost/algorithm/string/replace.hpp>
#include "Config.h"
#include "Settings.h"
#include "ServerException.h"
#include "Logger.h"
#include "Utils.h"

bool double_space(char lhs, char rhs)
{
    return ((lhs == rhs) && (lhs == ' ')); 
}

bool double_newline(char lhs, char rhs)
{
    return ((lhs == rhs) && (rhs == '\n')); 
}


RequestParser::RequestParser(void)
{

}


RequestParser::~RequestParser(void)
{
}




void RequestParser::parse(const std::vector<char>& input, std::map<std::string,std::string> & output)
{
    std::map<std::string,std::string> ret;
    std::string tmethod;
    std::string tversion;
    std::string turi;
    std::string tbody;

    std::auto_ptr<Settings> settings(Settings::CreateSettings(CFG_CONFIGFILE_PATH));

    if (input.empty()) throw(ServerException("Request exceptionally small, request declined",0,warning,MessageParsing));
    
    
    if (input.size() > settings->max_request_size) throw(ServerException("Request exceptionally large, request declined",0,warning,MessageParsing));

    std::string headers;
    std::string cinput(input.begin(),input.end());
    
    utils::split_by_body(cinput, headers, tbody);

    ret["Body"] = tbody;

    _unify(headers);

    std::vector<std::string> lines;

    RequestParser::_split_by_lines(headers,lines);

    RequestParser::_split_first_line(lines[0], tmethod, turi, tversion);
    
    int iter=0;
    int iterheaders=0;
    if (std::find(settings->allowed_requests.begin(), settings->allowed_requests.end(), tmethod) == settings->allowed_requests.end())
        throw(ServerException("Method exceptionally bad, request declined",0,warning,MessageParsing));
    ret["Method"] = tmethod;
    ret["URI"] = turi;
    ret["Version"] = tversion;

    std::vector<std::string> rheaders(settings->headers);
    for(std::vector<std::string>::const_iterator it = boost::next(lines.begin()); it != lines.end(); ++it)
    {
        iterheaders += _add(*it, rheaders,ret,settings->unrecognized_pattern,iter);
    }

    std::string sbuf = (settings->unrecognized_pattern == "")? " unrecognized headers, " : " declined headers, ";
    Logger::log_manual("Parsing of "+ ret["Method"] +" request completed with " +
                        std::to_string(iterheaders) + " recognized headers and " +
                        std::to_string(iter) + sbuf +
                        "total request size: " + std::to_string(input.size()) + " bytes",
                        0, normal, MessageParsing);

    output.swap(ret);

}


void RequestParser::output(const std::map<std::string,std::string> &input)
{
    
    std::cout << "Starting line:" << "\r\n";
    std::cout << input.at("Method" )<<" " << input.at("URI") << " " << input.at("Version") << "\r\n";
    std::cout << "Headers:" << "\r\n";
    for(std::map<std::string, std::string>::const_iterator iter = input.begin(); iter != input.end(); ++iter)
    {
        if(iter->first!="Method"&&iter->first!="URI"&&iter->first!="Version"&&iter->first!="Body")
        {
            std::cout << iter->first <<": "<< iter->second << "\r\n";
        }
    }
    std::cout << "Body:" << "\r\n";
    std::cout << input.at("Body") << "\r\n";
}



bool RequestParser::_add(const std::string &input,
                   std::vector<std::string>& headers, 
                   std::map<std::string,std::string>& output, 
                   const std::string &unrec_pattern,
                   int & iter)
{
    for(std::vector<std::string>::const_iterator it = headers.begin(); it != headers.end(); ++it)
    {
        if(boost::starts_with(input,*it + ":"))
        {
            output[*it] = (input.length()>(it->length()+1)) ? input.substr(it->length()+2) : "";
            headers.erase(it);
            return true;
        }

    }
    ++iter;

    if (unrec_pattern == "") return false;

    output[unrec_pattern + std::to_string(iter-1)] = input;
    
    return false;
}



void RequestParser::_unify(std::string &input) 
{
    std::string sbuf = input;
    std::replace(sbuf.begin(), sbuf.end(),'\r','\n');

    std::string::iterator new_end = std::unique(sbuf.begin(), sbuf.end(), double_newline);
    sbuf.erase(new_end, sbuf.end());

    utils::replace(sbuf, "\n\t", " ");
    utils::replace(sbuf, "\n ", " ");

    new_end = std::unique(sbuf.begin(), sbuf.end(), double_space);
    sbuf.erase(new_end, sbuf.end());

    input.swap(sbuf);

}

void RequestParser::_split_by_lines(const std::string &input, std::vector<std::string> &out_lines)
{
    boost::split(out_lines, input, boost::is_any_of("\n"), boost::token_compress_on );
    if (out_lines.size() < 2) throw(ServerException("Exceptionally not enough lines, request declined",0,warning,MessageParsing));

}

void RequestParser::_split_first_line(const std::string &line,
                                     std::string &out_method, 
                                     std::string &out_URI, 
                                     std::string &out_version)
{
    std::vector<std::string> buffervec;
    boost::split(buffervec, line, boost::is_any_of(" "), boost::token_compress_on);

    if (buffervec.size() != 3) throw(ServerException("Exceptionally bad first line, request declined",0,warning,MessageParsing));

    out_method = buffervec[0];
    out_URI = buffervec[1];
    out_version = buffervec[2];

}

