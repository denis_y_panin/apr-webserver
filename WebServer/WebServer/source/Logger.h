#ifndef _LOGGER_
#define _LOGGER_

#pragma warning( disable: 4290 )
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/async_frontend.hpp>
#include <libconfig\libconfig.h++>

#include "ServerException.h"

class Logger
{
public:
    Logger(void);
    ~Logger(void);
    static void log_exception(const ServerException &exception);
    static void log_exception(const libconfig::ConfigException &exception);
    static void log_manual(
                    std::string message, 
                    int error, 
                    severity_level severity,
                    server_module module); 
    static void log_manual_tagged(
                    std::string message, 
                    int error, 
                    severity_level severity,
                    server_module module,
                    std::string tag); 
    static void log_manual(std::string message); //error 0, normal, general
    static void log_manual(std::string message, server_module module); // //error 0, normal
    static void log_manual_tagged(std::string message, std::string tag = "FANCY"); //error 0, normal, general
    static void log_manual_tagged(std::string message, server_module module, std::string tag = "FANCY"); // //error 0, normal
    static int init(); 
    static void shutdown();
private:
    static boost::shared_ptr< boost::log::sinks::asynchronous_sink< boost::log::sinks::text_file_backend > > _make_sink(
                                                                                                                       std::string name_template,
                                                                                                                       std::string path_to_log_dir,
                                                                                                                       unsigned int rotate_on_bytes,
                                                                                                                       const boost::log::formatter &fmt);
};
#endif //!_LOGGER_
