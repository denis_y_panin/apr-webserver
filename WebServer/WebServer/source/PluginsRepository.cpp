#include "PluginsRepository.h"
#include "Config.h"
#include "Settings.h"
#include "Logger.h"
#include <algorithm>
#include <boost/lexical_cast.hpp>

typedef IResponseGenerator* (*PLUGIN_CREATE)();

std::map<std::string, std::shared_ptr<IResponseGenerator>> PluginsRepository::loaded_plugins;
std::vector<HMODULE> PluginsRepository::plugin_handles;

PluginsRepository::PluginsRepository()
{
}

PluginsRepository::~PluginsRepository()
{
}

void PluginsRepository::load()
{
    if(!loaded_plugins.empty() && !plugin_handles.empty())
        clean_up();
    
    std::string path_to_plugins = Settings::CreateSettings(CFG_CONFIGFILE_PATH)->path_to_plugins;
    std::wstring search_path = std::wstring(path_to_plugins.begin(), path_to_plugins.end()) + L"\\*.dll";
    WIN32_FIND_DATA find_data;
    HANDLE result = FindFirstFile(search_path.data(), &find_data);
    BOOL next_result = TRUE;

    while (result != INVALID_HANDLE_VALUE && next_result)
    {
        std::wstring plugin_dll_name (find_data.cFileName);
        std::wstring plugin_full_name = std::wstring(path_to_plugins.begin(), path_to_plugins.end())  + L"\\" + plugin_dll_name;
        HMODULE h_mod = LoadLibrary(plugin_full_name.data());

        if(h_mod != NULL)
        {
            plugin_handles.push_back(h_mod);
            PLUGIN_CREATE p_create_function = (PLUGIN_CREATE) GetProcAddress(h_mod, "Create_Plugin");

            if(p_create_function != NULL)
            {
                std::shared_ptr<IResponseGenerator> plugin_ptr(p_create_function());
                std::string plugin_name = plugin_ptr->get_name();
                loaded_plugins[plugin_name] = plugin_ptr;
            }
            else
                throw ServerException("While loading plugin" + std::string(plugin_dll_name.begin(), plugin_dll_name.end())
                    + " Create function wasn't found", 0, warning, Plugins);
        }
        else 
            throw ServerException("Loading plugin " + std::string(plugin_dll_name.begin(), plugin_dll_name.end())
                + " failed", 0, warning, Plugins);

        next_result = FindNextFile (result, &find_data);
    }
    
    Logger::log_manual("Plugins were loaded successfully", 0, notification, Plugins);
}

void PluginsRepository::clean_up()
{
    loaded_plugins.clear();
    std::for_each(plugin_handles.begin(), plugin_handles.end(), [] (HMODULE h_mod)
    {
        FreeLibrary(h_mod);
    });
    plugin_handles.clear();
    
    Logger::log_manual("Plugins were unloaded", 0, notification, Plugins);
}