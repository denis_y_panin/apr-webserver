#ifndef I_WIN_SOCK_WRAPPER
#define I_WIN_SOCK_WRAPPER

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN  
#endif 

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vector>  

class IWinSockWrapper
{
public:
	int load(SOCKET ClientSocket, unsigned BufferLength)
    {
        return _load(ClientSocket, BufferLength);
    }
	int send_message(const std::vector<char> &message)
    {
        return _send_message(message);
    }
	int recieve_message(std::vector<char> &message)
    {
        return _recieve_message(message);
    }
    int close()
    {
        return _close();
    }
    IWinSockWrapper()
    {
    }
    virtual ~IWinSockWrapper()
    {
    }

private:
    virtual int _load(SOCKET ClientSocket, unsigned BufferLength)=0;
	virtual int _send_message(const std::vector<char>&)=0;
	virtual int _recieve_message(std::vector<char>&)=0;
    virtual int _close()=0;
};

#endif //!I_WIN_SOCK_WRAPPER
