#ifndef _IPLUGIN_
#define _IPLUGIN_

#include <string>
#include <vector>
#include <map>
#include <memory>

class IResponseGenerator
{
public:
    virtual void process_request(const std::map<std::string, std::string>& request, std::vector<char> &output, const std::string &root_path = "/") = 0;
    virtual const char* get_name() const = 0;
    virtual const char* get_version() const = 0;
    IResponseGenerator(void)
    {
    }
    virtual ~IResponseGenerator(void)
    {
    }
};
#endif //!_IPLUGIN_
