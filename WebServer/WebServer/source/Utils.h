#pragma once
#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include <algorithm>

namespace utils
{
    static void replace(std::string& input, const std::string& lhs, const std::string& rhs)
    {
        size_t begin = 0;
        while((begin = input.find(lhs, begin)) != std::string::npos)
        {
            input.replace(begin, lhs.length(), rhs);
            begin += rhs.length(); 
        }
    }

    static bool check_version(const std::string &lhs, const std::string &rhs) //rhs should be >= lhs and valid
    {
        return true;
    }
    
    static void split_by_body(const std::string &input, std::string &out_headers, std::string &out_body)
    {
        std::string doubleCLRF("\r\n\r\n"); 
        std::string::const_iterator bodystart = std::search(input.begin(),input.end(),doubleCLRF.begin(),doubleCLRF.end());
        if (bodystart == input.begin()) throw(ServerException("First line exceptionally not found, request declined",0,warning,MessageParsing));
        if (bodystart == input.end()) throw(ServerException("Body exceptionally not found, request declined",0,warning,MessageParsing));

        out_headers = std::string(input.begin(), bodystart);
        out_body = std::string(bodystart+4, input.end());
    }
}
#endif //!UTILS_H