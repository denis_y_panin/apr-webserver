#include "ServerException.h"
#include "Utils.h"

ServerException::ServerException(void) : std::exception(""), _severity(warning), _module(General), _error(0)
{
}


ServerException::~ServerException(void)
{
}

ServerException::ServerException(const std::string& message, 
                    int inerror, 
                    severity_level severity,
                    server_module module)
                    : std::exception(message.c_str()), _severity(severity), _module(General), _error(inerror)
{

}

ServerException::ServerException(const std::string& message)
                    : std::exception(message.c_str()), _severity(warning), _module(General), _error(0)
{

}

ServerException::ServerException(const std::string& message, 
                    int inerror)
                    : std::exception(message.c_str()), _severity(warning), _module(General), _error(inerror)
{

}

ServerException::ServerException(const std::string& message, 
                    int error,
                    severity_level severity)
                    : std::exception(message.c_str()), _severity(severity), _module(General), _error(error)
{

}

severity_level ServerException::get_severity() const
{
    return _severity;
}
server_module ServerException::get_module() const
{
    return _module;
}
int ServerException::get_error() const
{
    return _error;
}
int ServerException::print() const
{
    return 0;
}