#include "WinSockWrapperSSL.h"
#include "ServerException.h"
#include "Settings.h"
#include "Config.h"
#include "Logger.h"
#include <iostream>

WinSockWrapperSSL::WinSockWrapperSSL(SOCKET client_socket, unsigned  buffer_length): 
	_client_socket(client_socket), _buffer_length(buffer_length)
{
    if (_client_socket == INVALID_SOCKET)
        throw ServerException("Load socket failed", 0, warning, SSLWrapper);
 
    _prepare_connection(); 
}


WinSockWrapperSSL::~WinSockWrapperSSL(void)
{
    _close();
    Logger::log_manual("SSL connection closed", 0, normal, SSLWrapper);
}

int WinSockWrapperSSL::_load(SOCKET client_socket, unsigned buffer_length)
{
    if (_client_socket == INVALID_SOCKET)
        throw ServerException("Load socket failed", 0, warning, SSLWrapper);
    
    _client_socket = client_socket;
    _buffer_length = buffer_length;
    _prepare_connection();
    return 0;
}

int WinSockWrapperSSL::_send_message(const std::vector<char> &message)
{
    int result = SSL_write(ssl, message.data(), message.size());
    if( result <= 0)
        throw ServerException("Send message failed", 0, warning, SSLWrapper);
    return 0;
}

int WinSockWrapperSSL::_recieve_message(std::vector<char> &message)
{
    std::vector<char> not_full_message;
    int result = 0, attempts = 0;
    size_t size = 0;

    message.resize(0);
    not_full_message.resize(_buffer_length);
    
    while((result == 0 || result == 1) && attempts < CFG_ALLOWED_INCOMPLETE_REQUESTS_COUNT)   // because of some browsers' whims
    {
        result = SSL_read(ssl, not_full_message.data(), _buffer_length);
        if(result < 0)
            throw ServerException("Receive message failed", 0, warning, SSLWrapper);
        message.insert(message.end(), not_full_message.begin(), not_full_message.end());
        size += result;
        message.resize(size);
        attempts++;
    }
    
    if(result <= 0)
        throw ServerException("Receive message failed", 0, warning, SSLWrapper);
   
    return 0;
}

int WinSockWrapperSSL::_close()
{
    SOCKET sd = SSL_get_fd(ssl);
    SSL_free(ssl);
    SSL_CTX_free(ctx);
    shutdown(sd, SD_BOTH);
    closesocket(sd);
    return 0;
}

void WinSockWrapperSSL::_prepare_connection()												// initializes OpenSSL, loads certificates and performs handshake
{
    try
    {
        _init_ssl();
        _load_certificates();
        _handshake();
    }
    catch(const ServerException&)
    {
        _close();
        Logger::log_manual("SSL connection closed with error", 0, notification, SSLWrapper);
        throw;
    }
    Logger::log_manual("SSL connection opened successfully", 0, normal, SSLWrapper);
}

void WinSockWrapperSSL::_init_ssl()
{
    CRYPTO_malloc_init();
    SSL_library_init();
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();
    
    ctx = SSL_CTX_new(SSLv23_server_method()); 
    std::auto_ptr<BIO> bio(BIO_new_file(Settings::CreateSettings(CFG_CONFIGFILE_PATH)->path_to_dhparams_file.c_str(), "r"));
    if (bio.get() == NULL) 
        throw ServerException("Couldn't open DH param file, connection closed", 0, warning, SSLWrapper);

    std::auto_ptr<DH> ret(PEM_read_bio_DHparams(bio.get(), NULL, NULL, NULL));
    if (SSL_CTX_set_tmp_dh(ctx, ret.get()) < 0)				
        throw ServerException("Couldn't set DH parameters, connection closed", 0, warning, SSLWrapper);

    std::auto_ptr<RSA> rsa(RSA_generate_key(1024, RSA_F4, NULL, NULL));
    if (!SSL_CTX_set_tmp_rsa(ctx, rsa.get())) 
        throw ServerException("Couldn't set RSA key, connection closed", 0, warning, SSLWrapper);

}

int verify_callback(int ok, X509_STORE_CTX* store)											// function for certificate verification
{
    std::string data;
    std::string err_message;
    
    if (!ok) 
    {
        std::auto_ptr<X509> cert(X509_STORE_CTX_get_current_cert(store));
        int depth = X509_STORE_CTX_get_error_depth(store);
        int err = X509_STORE_CTX_get_error(store);

        err_message = "Error with certificate at depth:" + depth;
        data.resize(255);
        X509_NAME_oneline(X509_get_issuer_name(cert.get()), &data[0], 255);
        err_message.append("\n\tIssuer:");
        err_message.append(data);
        X509_NAME_oneline(X509_get_subject_name(cert.get()), &data[0], 255);
        err_message.append("\n\tSubject:");
        err_message.append(data);
        err_message.append("\n\tError " + err);
        err_message.append(": ");
        err_message.append(X509_verify_cert_error_string(err));

        throw ServerException(err_message, 0, warning, SSLWrapper);
    }

    return ok;
} 

int password_callback(char* buffer, int num, int rwflag, void* userdata)					//function for password check
{
    if (static_cast<size_t>(num) < (strlen(PASSWORD) + 1)) 
        return(0);
    strcpy_s(buffer, num, PASSWORD);
    return strlen(PASSWORD);
}

void WinSockWrapperSSL::_load_certificates()
{ 
    int r = 0;
    std::auto_ptr<Settings> settings(Settings::CreateSettings(CFG_CONFIGFILE_PATH));
    SSL_CTX_set_default_passwd_cb(ctx, password_callback);

    r = SSL_CTX_use_certificate_file(ctx, settings->path_to_server_certificate.c_str(), SSL_FILETYPE_PEM);  // loads server certificate 
    if(r <= 0)
    	throw ServerException("Couldn't load certificate file", 0, warning, SSLWrapper);

    r = SSL_CTX_use_PrivateKey_file(ctx, settings->path_to_server_private_key.c_str(), SSL_FILETYPE_PEM);  // loads key
    if(r <= 0)
    	throw ServerException("Couldn't load file with private key, connection closed", 0, warning, SSLWrapper);				

    SSL_CTX_load_verify_locations(ctx, settings->path_to_ca_certificate.c_str(), 0);							// loads certification authority location
    if(r <= 0)
    	throw ServerException("Couldn't load certification authority location, connection closed", 0, warning, SSLWrapper);		

    SSL_CTX_set_verify_depth(ctx, 1);
    SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, verify_callback);
}

void WinSockWrapperSSL::_handshake()
{
    std::string err_message = "";
    ssl = SSL_new(ctx);

    SSL_set_fd(ssl, _client_socket);
    SSL_set_cipher_list(ssl,"ALL");

    int r = SSL_accept(ssl);															 // accepts client conncetion request and performs handshake
    if (r == -1) 
    {
        err_message.append("SSL_accept() returned " + r);
        err_message.append("\nError in SSL_accept():" + SSL_get_error(ssl, r));
        std::string error;
        error.resize(_buffer_length);
        ERR_error_string_n(ERR_get_error(), &error[0], _buffer_length);
        err_message.append("\nError: ");
        err_message.append(error);
        ERR_print_errors(ssl_bio.get());
        err_message.append("\nWSA: " +  WSAGetLastError());

        throw ServerException(err_message, 0, warning, SSLWrapper);
    }

    if (SSL_get_verify_result(ssl) != X509_V_OK)    
    {
        throw ServerException("Certificate failed verification, connection closed", 0, warning, SSLWrapper);
    } 
}
