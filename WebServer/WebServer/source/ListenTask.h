#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef LISTEN_TASK
#define LISTEN_TASK

#include <windows.h>
#include <winSock2.h>
#include <tbb/task.h>
#include <tbb/atomic.h>

class ListenTask : public tbb::task
{
public:
    ListenTask(unsigned port, bool SSL, tbb::atomic<bool>& continuation_flag)
        :_port(port), _SSL(SSL), _continuation_flag(continuation_flag)
    {}
    tbb::task* execute();
private:
    unsigned _port;
    bool _SSL;
    tbb::atomic<bool>& _continuation_flag;
};
#endif //!LISTEN_TASK