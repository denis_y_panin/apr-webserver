#ifndef WIN_SOCK_WRAPPER_SSL
#define WIN_SOCK_WRAPPER_SSL



#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN  
#endif 

#pragma comment (lib, "ssleay32.lib")
#pragma comment (lib, "libeay32.lib")

#include "IWinSockWrapper.h"
#include <openssl/bio.h> // BIO objects for I/O
#include <openssl/ssl.h> // SSL and SSL_CTX for SSL connections
#include <openssl/err.h> // Error reporting
#include <openssl/rand.h>
#include <memory>

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vector>  

#define PASSWORD "tsnrtp"

class WinSockWrapperSSL:public IWinSockWrapper
{
public:
    WinSockWrapperSSL(SOCKET client_socket, unsigned int buffer_length);
    ~WinSockWrapperSSL(void);
private:
    SOCKET& _client_socket;
    SSL* ssl;
    std::auto_ptr<BIO> ssl_bio;
    SSL_CTX* ctx;
    unsigned _buffer_length;
    
    int _load(SOCKET client_socket, unsigned buffer_length);
    int _send_message(const std::vector<char>& message);
    int _recieve_message(std::vector<char>& message);
    int _close();
    
    void _prepare_connection();
    void _init_ssl();
    void _load_certificates();
    void _handshake();
};

#endif