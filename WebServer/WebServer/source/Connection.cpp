#pragma warning( disable: 4244 )
#include "Connection.h"

#include <map>
#include <string>
#include <algorithm>
#include <regex>
#include <fstream>
#include "WinSockWrapper.h"
#include "WinSockWrapperSSL.h"
#include "ServerException.h"
#include "Logger.h"
#include "Settings.h"
#include "Config.h"
#include "Utils.h"
#include "PluginsRepository.h"
#include "RequestParser.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <tbb/task.h>

Connection::Connection(SOCKET ClientSocket, unsigned BufferLength, bool SSL = FALSE) 
    : _ClientSocket(ClientSocket), _BufferLength(BufferLength)
{
    if (SSL) 
    {
        wrapper.reset(new WinSockWrapperSSL(_ClientSocket,_BufferLength)); 
    }else
    {
        wrapper.reset(new WinSockWrapper(_ClientSocket,_BufferLength));
    }
}

Connection::~Connection(void)
{
}

int Connection::start()
{
    int result = 0;
    std::vector<char> message_array;
    std::string source = "", path = "";


    Logger::log_manual("Connection opened", 0, normal, MessageProcessing);
    try
    {
        std::vector<char> answer;
        std::map<std::string, std::string> parsed;
        
        result = wrapper->recieve_message(message_array);
        RequestParser::parse(message_array, parsed);

        if(find_route(parsed["URI"], source, path))
        {
            if(PluginsRepository::loaded_plugins.find(source) != PluginsRepository::loaded_plugins.end())
                PluginsRepository::loaded_plugins[source]->process_request(parsed, answer, path);
            else
                throw ServerException("Necessary plugin wasn't found", 0 , warning, MessageProcessing);
        }
        else
        {
            std::string headers = "";
            std::string body = "<!DOCTYPE html><html><body><h1>404 Not Found</h1></body></html>";
            std::ostringstream date_time;
    
            date_time.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%a, %d %b %Y %H:%M:%S GMT")));
            date_time << boost::posix_time::second_clock::universal_time();

            headers.append("HTTP/1.1 404 Not Found");
            headers.append("\r\nServer: MFS");
            headers.append("\r\nDate: " + date_time.str());
            headers.append("\r\nContent-Type: text/html");
            headers.append("\r\nContent-Length: " + boost::lexical_cast<std::string>(body.size()));
            headers.append("\r\n\r\n");
            headers.append(body);

            answer.insert(answer.end(), headers.begin(), headers.end());
        }

        if(parsed.find("Accept-Encoding") != parsed.end() && parsed["Accept-Encoding"].find("gzip") != std::string::npos)
            gzip_data(answer);

        wrapper->send_message(answer);
    }
    catch(const ServerException &ex)
    {
        Logger::log_exception(ex);
        Logger::log_manual("Connection closed with error", 0, notification, MessageProcessing);
    }
    Logger::log_manual("Connection closed", 0, normal, MessageProcessing);
    return result;
}

bool Connection::find_route(const std::string &url, std::string &source, std::string &path)
{
    std::auto_ptr<Settings> settings(Settings::CreateSettings(CFG_CONFIGFILE_PATH));
    std::smatch match_results;

    path = settings->path_to_routes;

    for(std::map<std::string, std::vector<std::string>>::const_iterator iter = settings->routes.begin(); iter != settings->routes.end(); ++iter)
    {
        std::regex reg_expr(iter->first);
        if(std::regex_match(url, match_results, reg_expr))
        {
            path += iter->second[1] + ((match_results[4].str() == "")? "/index.*" : match_results[4].str());
            source = iter->second[0];

            return true;
        }
    }
    return false;
}

void Connection::gzip_data(std::vector<char> &in_out_data)
{
    std::string headers, body, compressed_body, tmp;
    std::stringstream stream_to_encode;

    tmp.insert(tmp.begin(), in_out_data.begin(), in_out_data.end());
    utils::split_by_body(tmp, headers, body);
    stream_to_encode << body;
    headers += "\r\nContent-Encoding: gzip\r\n\r\n";

    std::ostringstream fout(std::ios::out|std::ios::binary);
    boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
    in.push( boost::iostreams::gzip_compressor());
    in.push(stream_to_encode);
    boost::iostreams::copy(in, fout);
    compressed_body = fout.str();

    in_out_data.clear();
    in_out_data.insert(in_out_data.end(), headers.begin(), headers.end());
    in_out_data.insert(in_out_data.end(), compressed_body.begin(), compressed_body.end());
}