#pragma once
#ifndef PLUGINS_REPOSITORY_H
#define PLUGINS_REPOSITORY_H

#include <IResponseGenerator.h>
#include <map>
#include <memory>
#include <vector>
#include <windows.h>


class PluginsRepository
{
public:
    static std::map<std::string, std::shared_ptr<IResponseGenerator>> loaded_plugins;

    PluginsRepository();
    static void load();
    static void clean_up();
    ~PluginsRepository();
private: 
    static std::vector<HMODULE> plugin_handles;
};

#endif 