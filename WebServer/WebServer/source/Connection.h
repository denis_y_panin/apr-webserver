#ifndef _CONNECTION_
#define _CONNECTION_
#include "IWinsockwrapper.h"
#include <boost/scoped_ptr.hpp>
#include <string>
class Connection
{
public:
    Connection(SOCKET ClientSocket, unsigned BufferLenght, bool SSL);
    int start();
    ~Connection(void);
private:
    SOCKET _ClientSocket;
    unsigned _BufferLength;
    boost::scoped_ptr<IWinSockWrapper> wrapper;
    bool find_route(const std::string &url, std::string &source, std::string &path);
    void gzip_data(std::vector<char> &in_out_data);
};
#endif //!_CONNECTION_

