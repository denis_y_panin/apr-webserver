#ifndef SERVER_EXCEPTION
#define SERVER_EXCEPTION

#include <exception>
#include <string>

enum severity_level
{
    normal,
    notification,
    warning,
    error,
    critical
};

enum server_module
{
    WSWrapper,
    SSLWrapper,
    MessageParsing,
    MessageProcessing,
    ServerModule,
    ConfigLoading,
    Plugins,
    General
};

class ServerException :
    public std::exception
{
public:
    ServerException(void);
    ~ServerException(void);

    ServerException(const std::string& message, 
                    int error, 
                    severity_level severity,
                    server_module module);
   
    ServerException(const std::string& message);

    ServerException(const std::string& message, 
                    int error);
                  
    ServerException(const std::string& message, 
                    int error,
                    severity_level severity);

    severity_level get_severity() const;
    server_module get_module() const;
    int get_error() const;
    int print() const;

private:
    severity_level _severity; 
    server_module _module;
    int _error;
};
#endif //!SERVER_EXCEPTION

