#include "ListenTask.h"
#include <tbb/task_group.h>
#include <boost/lexical_cast.hpp>
#include "Config.h"
#include "Connection.h"
#include "Settings.h"
#include "Logger.h"
#include "ServerException.h"

tbb::task* ListenTask::execute()
{
    SOCKET listen_socket = INVALID_SOCKET;
    SOCKET client_socket = INVALID_SOCKET;
	struct addrinfo *result = NULL;
    struct addrinfo hints;
    int func_result;
    ZeroMemory(&hints, sizeof(hints));
   
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    func_result = getaddrinfo(NULL, boost::lexical_cast<std::string, unsigned int>(_port).c_str(), &hints, &result);

    if ( func_result != 0 ) 
    {
        int wsaerror = WSAGetLastError();
        throw(ServerException("Listening on port " + std::to_string(_port) + " failed: " + "getaddrinfo failed with error", 
                                wsaerror, error, ServerModule));
    }
    
    listen_socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (listen_socket == INVALID_SOCKET) 
    {
        freeaddrinfo(result);
        int wsaerror = WSAGetLastError();
        throw(ServerException("Listening on port " + std::to_string(_port) + " failed: " + "socket failed with error", 
                                wsaerror, error, ServerModule));
    }

    func_result = bind( listen_socket, result->ai_addr, (int)result->ai_addrlen);
    if (func_result == SOCKET_ERROR) 
    {
        closesocket(listen_socket);
        int wsaerror = WSAGetLastError();
        throw(ServerException("Listening on port " + std::to_string(_port) + " failed: " + "bind failed with error", 
                                wsaerror, error, ServerModule));
    }

    freeaddrinfo(result);
    func_result = listen(listen_socket, SOMAXCONN);
    if (func_result == SOCKET_ERROR) 
    {
        closesocket(listen_socket); 
        int wsaerror = WSAGetLastError();
        throw(ServerException("Listening on port " + std::to_string(_port) + " failed: " + "listen failed with error", 
                                wsaerror, error, ServerModule));
    }
    Logger::log_manual("Listening started on port: " + std::to_string(_port), ServerModule);

    tbb::task_group connections;

    while(_continuation_flag)
    {

        client_socket = accept(listen_socket, NULL, NULL);
        if (client_socket == INVALID_SOCKET) 
        {
            closesocket(listen_socket);
            int wsaerror = WSAGetLastError();
            if(_continuation_flag) Logger::log_manual("Listening on port " + std::to_string(_port) + " failed: " + "accept failed with error", wsaerror, error, ServerModule);
        }
        else
        {
            connections.run([&]() //some lambda magic
            {
                try
                {
                    boost::scoped_ptr<Connection> newConnection(new Connection(client_socket, CFG_BUFLEN, _SSL));
                    newConnection->start();
                }
                catch(const ServerException &ex)
                {
                    Logger::log_exception(ex);
                    Logger::log_manual("Opening new connection failed", 0, notification, ServerModule);
                }
            });
        }
    }
    return NULL;
}