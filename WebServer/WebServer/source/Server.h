#pragma comment (lib, "Ws2_32.lib")

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef SERVER_H
#define SERVER_H

#include <windows.h>
#include <winSock2.h>
#include <tbb/atomic.h>
#include "ListenRoot.h"

#define BUFLEN 512
#define PORT "4080"

class Server
{
public:
	Server();
	int run();
	int stop();
	int restart();
	int get_info() const;
    ~Server();
private:
	int _load_config();
    ListenRoot* root;
    tbb::atomic<bool> _continuation_flag;
};
#endif //!SERVER
