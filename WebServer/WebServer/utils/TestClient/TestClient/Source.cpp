#define WIN32_LEAN_AND_MEAN
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/algorithm/string.hpp>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "4080"

void replace(std::string& input, const std::string& lhs, const std::string& rhs)
{
    size_t begin = 0;
    while((begin = input.find(lhs, begin)) != std::string::npos)
    {
        input.replace(begin, lhs.length(), rhs);
        begin += rhs.length(); 
    }
}

std::string load_data_in_str(std::string fname)
{
    boost::filesystem::ifstream fIn;
    fIn.open(fname.c_str(),std::ios::in);

    if (!fIn) {
        std::cout<<"Error reading contents of file"<<"\r\n";
        return std::string("Error");
    }

    std::string l;
    std::stringstream ss;
    ss << fIn.rdbuf();
    return ss.str();
}

void write_str_in_file(std::string fname, std::string data, bool append=false)
{
    boost::filesystem::ofstream fOut;

    if (append)
        fOut.open(fname.c_str(),std::ios::app);
    else
        fOut.open(fname.c_str(),std::ios::out);

    if (!fOut) {
        std::cout  << "Error writing to file: " << fname << "\r\n";
        return;
    }

    fOut << data;

    fOut.close();
}
int sendreq(const std::string &request, std::vector<char> &response,  SOCKET &ConnectSocket) 
{
    int iResult;
    int recvbuflen = DEFAULT_BUFLEN;
    if (response.size()<DEFAULT_BUFLEN)
        response.resize(DEFAULT_BUFLEN);
    iResult = send( ConnectSocket, request.c_str(), (int)strlen(request.c_str()), 0 );
    if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
    iResult = recv(ConnectSocket, response.data(), recvbuflen, 0);
    if ( iResult > 0 )
    {
        printf("Bytes received: %d\n", iResult);
        response.resize(iResult);
    }
    else if ( iResult == 0 )
        printf("Connection closed\n");
    else
        printf("recv failed with error: %d\n", WSAGetLastError());

   
    return iResult;
   
}
int connect(char * servname, char * port, SOCKET &ConnectSocket)
{
    WSADATA wsaData;
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;
    int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    iResult = getaddrinfo(servname, port, &hints, &result);
    if ( iResult != 0 ) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return 1;
    }
    for(ptr=result; ptr != NULL ;ptr=ptr->ai_next)
    {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
            WSACleanup();
            return 1;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    if (ConnectSocket == INVALID_SOCKET)
    {
        printf("Unable to connect to server!\n");
        WSACleanup();
        return 1;
    }

    freeaddrinfo(result);
    return 0;
}

void main(int argc, char* argv[])
{
   
    if (argc < 5)
    {
        std::cout<<"invalid args\r\n";
        std::cin.get();
        return;
    }
    boost::filesystem::path InputPath = boost::filesystem::system_complete(boost::filesystem::path(argv[3],boost::filesystem::native));
   
    if (!exists(InputPath)) {
        std::cout << "Error: directoty " << InputPath << " does not exist\r\n";
        std::cin.get();
        return;
    }

    if (!is_directory(InputPath)) {
        std::cout << InputPath << " is not a directory\r\n";
        std::cin.get();
        return;
    }
     std::vector<boost::filesystem::path> files;
    
    std::copy(boost::filesystem::directory_iterator(InputPath), boost::filesystem::directory_iterator(), back_inserter(files));
    SOCKET sk;
    if(connect(argv[1], argv[2], sk))
    {
        std::cin.get();
        return;
    }
    
    for (std::vector<boost::filesystem::path>::const_iterator it = files.begin(); it!=files.end();++it)
    {
        if (it->extension().generic_string()==".txt")
        {
            std::vector<char> torecv;
            torecv.resize(DEFAULT_BUFLEN);
            std::string tosend = load_data_in_str(it->generic_string());
            replace(tosend,"\n","\r\n");

            int ret = sendreq(tosend, torecv, sk);
            if (ret <= 0)
            {
                closesocket(sk);
                WSACleanup();
                std::cin.get();
                return;
            }

            std::string fnametw = argv[4];
            fnametw += it->filename().generic_string();

            std::string recvd(torecv.begin(),torecv.end());

            write_str_in_file(fnametw,recvd,false);
        }
    }
    closesocket(sk);
    WSACleanup();
    std::cout<<"Finished\r\n";
    std::cin.get();
}