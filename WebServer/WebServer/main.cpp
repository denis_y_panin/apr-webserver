#pragma warning( disable: 4290 )

#include "Server.h"
#include "Logger.h"
#include "ServerException.h"
#include <iostream>
#include <libconfig\libconfig.h++>


int main()
{
    try
    {
        Logger::init();
        Server server;
        server.run();
    }
    catch(const libconfig::ConfigException &ex)
    {
        Logger::log_exception(ex);
	    Logger::log_manual("Configuration file failed to load", 0, notification, ConfigLoading);
    }
    catch(const ServerException &ex)
    {
        Logger::log_exception(ex);
    }
    
	return 0;
}