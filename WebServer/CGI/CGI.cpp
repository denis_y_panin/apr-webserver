// CGI.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "IResponseGenerator.h"
#include "CGIGenerator.h"


extern "C"
{
    // Plugin factory function
    __declspec(dllexport) IResponseGenerator* Create_Plugin ()
    {
        return new CGIGenerator ();
    }
}

