#pragma once
#include "IResponseGenerator.h"
#include <memory>
#include <unordered_map>
#include <boost/filesystem.hpp>
class CGIGenerator 
    : public IResponseGenerator
{
public:
    CGIGenerator(void);
    virtual ~CGIGenerator(void);
    virtual void process_request(const std::map<std::string, std::string> &request, std::vector<char> &output, const std::string &root_path = "/");
    virtual const char* get_name() const;
    virtual const char* get_version() const;
    void generate_error_response(std::vector<char> &output, const std::string &error_code);
    
private:
    void add_common_headers(std::string &output, const std::string &filepath = "");
    void generate_get_response(std::vector<char> &output, const std::vector<char> &response);
    int generate(const std::string& input, std::vector<char> &output, LPCWSTR path);
};

