#include "stdafx.h"
#include "CGIGenerator.h"

#include <fstream>
#include <locale>
#include <string>
#include <sstream>
#include <regex>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/algorithm/string/find.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lexical_cast.hpp>

#define BUFSIZE 4096 
#define PATH L"plugins\\CGIDemo.exe"

const char* CGIGenerator::get_name() const
{
    return "CGI";
}

const char* CGIGenerator::get_version() const
{
    return "1.0";
}

void CGIGenerator::process_request(const std::map<std::string, std::string> &request, std::vector<char> &output, const std::string &root_path)
{
    std::vector<char> answer;
    if(generate(request.at("URI"), answer, PATH))
    {
        generate_get_response(output, answer);
    }else
        output = answer;

}

void CGIGenerator::add_common_headers(std::string &output, const std::string &filepath)
{
    std::ostringstream date_time;
    
    date_time.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%a, %d %b %Y %H:%M:%S GMT")));
    date_time << boost::posix_time::second_clock::universal_time();

    output.append("\r\nDate: " + date_time.str());
    output.append("\r\nServer: MFS");
    output.append("\r\nContent-Type: text/html");
}

void CGIGenerator::generate_get_response(std::vector<char> &output, const std::vector<char> &response)
{
    std::string answer_headers = "";
    std::ostringstream date_time;
    
    date_time.imbue(std::locale(std::cout.getloc(), new boost::posix_time::time_facet("%a, %d %b %Y %H:%M:%S GMT")));
    answer_headers.append("HTTP/1.1 200 OK");

    add_common_headers(answer_headers);
    std::string len = "\r\nContent-Length: " + std::to_string(response.size());
    answer_headers.append(len);
    answer_headers.append("\r\n\r\n");

    output.insert(output.begin(), answer_headers.begin(), answer_headers.end());

    output.insert(output.end(),response.begin(), response.end());

}

int CGIGenerator::generate(const std::string& input, std::vector<char> &output, LPCWSTR path)
{
    HANDLE g_hChildStd_IN_Rd = NULL;
    HANDLE g_hChildStd_IN_Wr = NULL;
    HANDLE g_hChildStd_OUT_Rd = NULL;
    HANDLE g_hChildStd_OUT_Wr = NULL;

    SECURITY_ATTRIBUTES saAttr; 

    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
    saAttr.bInheritHandle = TRUE; 
    saAttr.lpSecurityDescriptor = NULL; 

    if ( !CreatePipe(&g_hChildStd_OUT_Rd, &g_hChildStd_OUT_Wr, &saAttr, 0) ||
         !SetHandleInformation(g_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0) ||
         !CreatePipe(&g_hChildStd_IN_Rd, &g_hChildStd_IN_Wr, &saAttr, 0)   ||
         !SetHandleInformation(g_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0) ) 
    {
        generate_error_response(output, "CGI interpreter connection error");
        return -1;
    }

    //opening process of interpreter
    PROCESS_INFORMATION piProcInfo; 
    STARTUPINFO siStartInfo;
    BOOL bSuccess = FALSE; 
 
    ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) );
 
    ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) );
    siStartInfo.cb = sizeof(STARTUPINFO); 
    siStartInfo.hStdError = g_hChildStd_OUT_Wr;
    siStartInfo.hStdOutput = g_hChildStd_OUT_Wr;
    siStartInfo.hStdInput = g_hChildStd_IN_Rd;
    siStartInfo.dwFlags |= STARTF_USESTDHANDLES;
    
    bSuccess = CreateProcess(path, 
        NULL,     // command line 
        NULL,          // process security attributes 
        NULL,          // primary thread security attributes 
        TRUE,          // handles are inherited 
        0,             // creation flags 
        NULL,          // use parent's environment 
        NULL,          // use parent's current directory 
        &siStartInfo,  // STARTUPINFO pointer 
        &piProcInfo);  // receives PROCESS_INFORMATION 
   
    if ( ! bSuccess ) 
    {
        generate_error_response(output, "CGI interpreter not found"); 
        return -1;
    }
    else 
    {
        CloseHandle(piProcInfo.hProcess);
        CloseHandle(piProcInfo.hThread);
    }

    //sending request
    DWORD dwRead, dwWritten; 
    CHAR chBuf[BUFSIZE];

    bSuccess = WriteFile(g_hChildStd_IN_Wr, input.data(), input.size(), &dwWritten, NULL);
 
    if ( ! CloseHandle(g_hChildStd_IN_Wr) ) 
    {
        generate_error_response(output, "CGI interpreter connection error"); 
        return -1;
    }

    CloseHandle(g_hChildStd_OUT_Wr);
   //receiving 

    HANDLE hParentStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    std::string outputbuff;

    for (;;) 
    { 
        bSuccess = ReadFile( g_hChildStd_OUT_Rd, chBuf, BUFSIZE, &dwRead, NULL);
        if( ! bSuccess || dwRead == 0 ) break; 
        outputbuff += std::string(chBuf,dwRead);
        if (! bSuccess ) break; 
    }

    CloseHandle(g_hChildStd_OUT_Rd);

    //final output

    output = std::vector<char>(outputbuff.begin(),outputbuff.end());
    return 1;

}

void CGIGenerator::generate_error_response(std::vector<char> &output, const std::string &error_code)
{
    std::string answer = "";
    std::string body = "<!DOCTYPE html><html><body><h1>"+ error_code + "</h1></body></html>";

    answer.append("HTTP/1.1 "+ error_code);
    add_common_headers(answer);
    answer.append("\r\nContent-Length: " + boost::lexical_cast<std::string>(body.size()));
    answer.append("\r\n\r\n");
    answer.append(body);

    output.insert(output.end(), answer.begin(), answer.end());
}

CGIGenerator::CGIGenerator(void)
{
    
}


CGIGenerator::~CGIGenerator(void)
{
}