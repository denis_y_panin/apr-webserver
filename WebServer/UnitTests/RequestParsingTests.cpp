#include "gtest/gtest.h"
#include "source\ServerException.h"
#include "source\ServerException.cpp"
#include "source\RequestParser.h"
#include "source\RequestParser.cpp"
#include "source\Settings.h"
#include "source\Settings.cpp"
#include "source\Logger.cpp"

TEST(loadRequest, emptyRequestTest)
{
    std::vector<char> test_request;
    std::map<std::string,std::string> result;
    EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, emptyLinesTest)
{
	std::string str = "\r\n\r\n\r\n";
	std::vector<char> test_request(str.begin(), str.end());
	std::map<std::string,std::string> result;
	EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, emptyHeadersTest)
{
    std::string str = "GET /wiki/HTTP HTTP/1.1\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, missingStartLineTest)
{
    std::string str = "Host: ru.wikipedia.org\r\nDate: Thu, 19 Feb 2009 11:08:01 GMT\r\nContent-Type: text/html; charset=windows-1251\r\n\r\n"+
		std::string("<html><body><a href='http://example.com/about.html#contacts/'>Click here</a></body></html>");
    std::vector<char> test_request(str.begin(), str.end());
	std::map<std::string,std::string> result;
	EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, messingEndlTest)
{
    std::string str = "GET /wiki/HTTP HTTP/1.0\r\nHost: ru.wikipedia.org";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, missingEmptyLineTest)
{
    std::string str = "GET /wiki/HTTP HTTP/1.0\r\nHost: ru.wikipedia.org\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, badFirstLineTest)
{
    std::string str = "GET HTTP/1.1\r\nHost: ru.wikipedia.org\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, unifyTest)
{
    std::string str = std::string("GET / HTTP/1.1\r\nHost: \r\n ru.wikipedia.org\r\nDate: Thu, 19 Feb 2009 11:08:01 GMT\r\n")+
		std::string("Content-Type: text/html; charset=windows-1251\r\n\r\n<html><body><a href='http://example.com/about.html#contacts/'>Click here</a></body></html>");
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	RequestParser::parse(test_request, result);
	EXPECT_EQ("GET", result["Method"]);
	EXPECT_EQ("HTTP/1.1", result["Version"]);
	EXPECT_EQ("/", result["URI"]);
	EXPECT_EQ("ru.wikipedia.org", result["Host"]);
	EXPECT_EQ("<html><body><a href='http://example.com/about.html#contacts/'>Click here</a></body></html>", result["Body"]);
}

TEST(loadRequest, wrongMethodTest)
{
    std::string str = "GETTT /wiki/HTTP HTTP/1.0\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
    EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, unsupportedMethodTest)
{
    std::string str = "PUT /wiki/HTTP HTTP/1.0\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
    EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, wrongVersionTest)
{
    std::string str = "GETTT /wiki/HTTP HTTP/1\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
    EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, wrongURITest)
{
    std::string str = "GET /|wiki/HTTP HTTP/1\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
    EXPECT_THROW(RequestParser::parse(test_request, result), ServerException);
}

TEST(loadRequest, missingDelimiterInHeaderTest)
{
    std::string str = "GET /wiki/HTTP HTTP/1.0\r\nHost: ru.wikipedia.org\r\nAccept text/html\r\n\r\n";
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	RequestParser::parse(test_request, result);
    EXPECT_THROW(result.at("Accept"), std::exception);
}

TEST(loadRequest, unrecognizedHeaderTest)
{
    std::string str = "GET / HTTP/1.1\r\nHost: 127.0.0.1:4531\r\nConnection: keep-alive\r\nCache-Control: max-age=0\r\n" +
		std::string("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nWrongHeader: value\r\n") +
		std::string("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36\r\n") +
		std::string("Accept-Encoding: gzip,deflate,sdch\r\nAccept-Language: en-US,en;q=0.8\r\n\r\n");
    std::vector<char> test_request(str.begin(), str.end());
    std::map<std::string,std::string> result;
	RequestParser::parse(test_request, result);
	EXPECT_EQ("127.0.0.1:4531", result["Host"]);
	EXPECT_EQ("keep-alive", result["Connection"]);
	EXPECT_EQ("max-age=0", result["Cache-Control"]);
	EXPECT_EQ("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36", result["User-Agent"]);
	EXPECT_EQ("en-US,en;q=0.8", result["Accept-Language"]);
	EXPECT_EQ("gzip,deflate,sdch", result["Accept-Encoding"]);
	EXPECT_EQ("max-age=0", result["Cache-Control"]);
    EXPECT_THROW(result.at("WrongHeader"), std::exception);
}